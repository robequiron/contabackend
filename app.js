//Imports
const express = require("express");
const mongoose = require("mongoose");

//Instantiate
const app = express();

//Routes


app.get("/", (req,res)=>{
    res.status(200).json({
        ok:true,
        mensaje:"Raíz correcta",
    })
})

/*===========================================
/  Connection to the database                                
/*=========================================*/

mongoose.connection.openUri("mongodb://localhost:27017/conta", (err,res)=>{
    if(err) { throw err} 

    if(res) {
        console.log("Conección a la base de datos");
    }
})

/*===========================================
/  Listening server                                
/*=========================================*/
app.listen(3000, ()=>{
    console.log("Servidor en el puerto 3000");
})