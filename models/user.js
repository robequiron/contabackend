var mongoose = require("mongoose");

var Schema = mongoose.Schema;

/*===========================================
/  User model                                
/*=========================================*/
var userSchema = new Schema({
    name: {type:String, required: [true, "El usuario es obligatorio"]},
    img: {type:String, required:false},
    rol: {type:String, default:'USER_ROL',required:[true, "Es necesario un rol"]}, 
    email: {type:String, unique:true, required:[true,"Es necesario un email"]},
    password: {type:String, required:[true, "Es necesario un password"]}
})

/*===========================================
/  Export model                                
/*=========================================*/
module.exports = mongoose.model('User', userSchema);

